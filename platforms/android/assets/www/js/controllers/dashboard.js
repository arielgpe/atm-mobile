'use strict';


app.controller('DashboardController', function($scope, $location, $ionicModal, $ionicPopover, $ionicViewService, $ionicPopup, Scanner, ScannerLock, $cordovaPinDialog){
	$scope.total = {
		assigned: 1785369.30,
		sold: 2852136.75,
		remaining: 975369.13
	};
	var box = localStorage.getItem('tutorialBox');
    var pinPassword = localStorage.getItem('pinPassword');
	var tb = JSON.parse(box);

	if (tb != null ) {
		$scope.tutorialBox = tb;
	} else {
		localStorage.setItem('tutorialBox', true);
	}
	
	$scope.pinTest = function() {
		$cordovaPinDialog.prompt('Please insert your password', function(results) {
            if (results.buttonIndex == 1) {
				console.log(results.input1)
			} else {
				console.log('cancel')
			}
		}, 'Security Password');
	};

	$scope.scanBarcode = function() {
		if (JSON.parse(box) === false) {
            if (pinPassword) {
                ScannerLock();
            } else {
                $location.path('empty');
                Scanner();
            }

		} else {
			$location.path('message');

		}
	};

	$scope.scanBarcodeContinue = function() {
		$ionicViewService.nextViewOptions({
			disableBack: true
		});
		$location.path('dashboard').replace();
        if (pinPassword) {
            ScannerLock();
        } else {
            Scanner();
        }
	};

	$scope.checkBox = function() {
		$scope.tutorialBox = !$scope.tutorialBox;
		localStorage.setItem('tutorialBox', JSON.parse($scope.tutorialBox));
	};

	$scope.logout = function() {
		localStorage.clear();
		$ionicViewService.nextViewOptions({
			disableBack: true
		});
		$location.path('login').replace();
	};

	$scope.settings = function() {
		
		$location.path('settings');
	};


	$ionicPopover.fromTemplateUrl('my-popover.html', {
		scope: $scope
    }).then(function(popover) {
		$scope.popover = popover;
	});

	$scope.openPopover = function($event) {
		$scope.popover.show($event);
	};

	$scope.closePopover = function() {
		$scope.popover.hide();
	};

	$scope.$on('$destroy', function() {
		$scope.popover.remove();
	});

	$scope.$on('popover.hidden', function() {

	});

	$scope.$on('popover.removed', function() {

	});
});