'use strict';

app.controller('SettingsController', function($scope, $window, $location, $cordovaPinDialog, $ionicPopup, $timeout){
    var checkboxTutorial = localStorage.getItem('tutorialBox');
    var checkboxLock = localStorage.getItem('lockBox');
    var pinPassword = localStorage.getItem('pinPassword');
    var pinStatus = localStorage.getItem('pinStatus');
    $scope.checkboxPin = JSON.parse(checkboxLock);
    $scope.checkbox = JSON.parse(checkboxTutorial);
    if (pinStatus == undefined || pinStatus == null) {
        $scope.pinStatus = 'Off';
    } else {
        $scope.pinStatus = pinStatus;
    }

    $scope.prueba = function() {
        $scope.checkbox = !$scope.checkbox;
        $window.localStorage['tutorialBox'] = $scope.checkbox;
    };

    $scope.goDashboard = function() {

        $location.path('dashboard').replace();
    };

    $scope.golockSettings = function () {
        if (pinPassword) {
            $cordovaPinDialog.prompt('Please insert your password', function (results) {
                if (results.buttonIndex == 1) {
                    console.log(results.input1);
                    if (results.input1 == pinPassword) {
                        $timeout(function () {
                            $location.path('settings/lock').replace();
                        }, 100);
                    } else {
                        $ionicPopup.alert({
                            title: 'Oops!',
                            template: "<p style='text-align: center'>Wrong Password</p>"
                        }).then(function (result) {
                            if (result == true) {
                                console.log($scope.checkboxPin);
                            }
                        });
                    }
                } else {
                    console.log('cancelled')
                }
            }, 'Security Password');
        } else {
            $location.path('settings/lock').replace();
        }
    };
});

app.controller('LockController', function ($scope, $cordovaPinDialog, $ionicPopup, $location, $timeout) {

    $scope.noPassword = function () {
        localStorage.setItem('pinStatus', 'Off');
        localStorage.setItem('pinPassword', '');
        $location.path('settings').replace();
    };

    $scope.goSettings = function() {

        $location.path('settings').replace();
    };

    $scope.lockPassword = function () {
        $cordovaPinDialog.prompt('Please insert your new password', function (results) {
            if (results.buttonIndex == 1) {
                console.log(results.input1);
                $cordovaPinDialog.prompt('Confirm your new password', function (result2) {
                    if (result2.buttonIndex == 1) {
                        if (result2.input1 == results.input1) {
                            var resulting = result2.input1;
                            localStorage.setItem('pinStatus', 'On');
                            localStorage.setItem('pinPassword', resulting);

                            $timeout(function () {
                                $location.path('settings').replace();

                            }, 100);
                        } else {
                            $ionicPopup.alert({
                                title: 'Oops!',
                                template: "<p style='text-align: center'>Passwords do not match!</p>"
                            })
                        }
                    } else {
                        console.log('cancelled')
                    }
                }, 'Security Password');
            } else {
                console.log('cancelled')
            }
        }, 'Security Password');
    };
});