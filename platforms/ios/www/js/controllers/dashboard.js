'use strict';

function tryToScan($cordovaBarcodeScanner, $cordovaToast) {
	$cordovaBarcodeScanner.scan().then(function(imageData) {
		if (imageData.cancelled) {
			return null;
		} else {
			$cordovaToast.show(imageData.text + '\n' + imageData.format, 'long', 'bottom').then(function(success) {

			}, function(error) {

			})
			
			return tryToScan($cordovaBarcodeScanner, $cordovaToast);
		}

	}, function(err) {
		$cordovaToast.show(err, 'long', 'bottom').then(function(success) {

		}, function(error) {

		})
		return null;
	});

};

app.controller('DashboardController', function($scope, $location, $ionicPopover, $ionicViewService, $cordovaBarcodeScanner, $cordovaToast, $cordovaPinDialog){
	$scope.total = {
		assigned: 1785369.30,
		sold: 2852136.75,
		remaining: 975369.13
	};
	var box = localStorage.getItem('tutorialBox');
	var tb = JSON.parse(box);

	if (tb != null ) {
		$scope.tutorialBox = tb;
	} else {
		localStorage.setItem('tutorialBox', true);
	}
	
	$scope.pinTest = function() {
		$cordovaPinDialog.prompt('Los tigueres', function(results) {
			if (results.buttonIndex == 1) {
				console.log(results.input1)
			} else {
				console.log('cancel')
			}
		}, 'Mis hermanos');
	};

	$scope.scanBarcode = function() {
		// console.log($scope.tutorialBox);
		if (JSON.parse(box) === false) {
			tryToScan($cordovaBarcodeScanner, $cordovaToast);

		} else {
			$location.path('message');

		}
	};

	$scope.scanBarcodeContinue = function() {
		$location.path('dashboard').replace();
		tryToScan($cordovaBarcodeScanner, $cordovaToast);
	}

	$scope.checkBox = function() {
		$scope.tutorialBox = !$scope.tutorialBox;
		localStorage.setItem('tutorialBox', JSON.parse($scope.tutorialBox));
	};

	$scope.logout = function() {
		localStorage.clear();
		$ionicViewService.nextViewOptions({
			disableBack: true
		});
		$location.path('login');
	};


	$ionicPopover.fromTemplateUrl('my-popover.html', {
		scope: $scope,
	}).then(function(popover) {
		$scope.popover = popover;
	});

	$scope.openPopover = function($event) {
		$scope.popover.show($event);
	};

	$scope.closePopover = function() {
		$scope.popover.hide();
	};

	$scope.$on('$destroy', function() {
		$scope.popover.remove();
	});

	$scope.$on('popover.hidden', function() {

	});

	$scope.$on('popover.removed', function() {

	});
});