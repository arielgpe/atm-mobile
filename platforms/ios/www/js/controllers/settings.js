'use strict';

app.controller('SettingsController', function($scope, $window){
	var checkboxVar = localStorage.getItem('tutorialBox');
	$scope.checkbox = JSON.parse(checkboxVar);


	$scope.prueba = function() {
		$scope.checkbox = !$scope.checkbox;
		$window.localStorage['tutorialBox'] = $scope.checkbox;
	}
});