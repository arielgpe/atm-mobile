'use strict';

app.factory('LoginService', function($resource, BACKEND_URL){
	return $resource(BACKEND_URL + 'auth/login.json');
});

app.factory('RegisterService', function($resource, BACKEND_URL){
	return $resource(BACKEND_URL + 'auth/register.json');
});