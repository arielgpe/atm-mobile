// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', 
  [
  'ionic', 
  'starter.controllers', 
  'starter.services',
  'ngResource',
  'ngCordova'
  ]);

app.constant('BACKEND_URL', 'http://192.168.1.181/ionic/MRPr/backend/public/');

app.run(function($ionicPlatform, $rootScope, $location, $window) {


 $rootScope.$on('$locationChangeStart', function(event, next, current) {
    if (localStorage.getItem("KEEP_LOGIN") == undefined || localStorage.getItem("KEEP_LOGIN") == null || localStorage.getItem("KEEP_LOGIN") == '') {
      if($window.location.hash === '#/login' || $window.location.hash === '#/register') {
        console.log($window.location);
      } else {
        $location.path('login');
      }
    } else if (localStorage.getItem("KEEP_LOGIN") && $window.location.hash === '#/login' || $window.location.hash === '#/register' || $window.location.hash === '#/forgot') {
      $location.path('dashboard');
    }
  });


  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
  if(window.cordova && window.cordova.plugins.Keyboard) {
    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
  }
  if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
});

app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    //set up an non abstract state for the login

    .state('login', {
      url: "/login",
      abstract: false,
      templateUrl: "templates/auth/login.html",
      controller: 'LoginController'
    })

    .state('register', {
      url: "/register",
      abstract: false,
      templateUrl: "templates/auth/register.html",
      controller: 'RegisterController'
    })

    .state('forgot', {
      url: "/forgot",
      abstract: false,
      templateUrl: "templates/auth/forgot.html",
      controller: 'ForgotController'
    })

    //state of the current app

    .state('dashboard', {
      url: "/dashboard",
      abstract: false,
      templateUrl: "templates/dashboard.html",
      controller: "DashboardController"
    })

    .state('message', {
      url: "/message",
      abstract: false,
      templateUrl: "templates/message.html",
      controller: "DashboardController"
    })

    .state('settings', {
      url: "/settings",
      abstract: false,
      templateUrl: "templates/index.html",
      controller: "SettingsController"
    })

    // setup an abstract state for the tabs directive
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    })

    // Each tab has its own nav history stack:

    .state('tab.dash', {
      url: '/dash',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tab-dash.html',
          controller: 'DashCtrl'
        }
      }
    })

    .state('tab.friends', {
      url: '/friends',
      views: {
        'tab-friends': {
          templateUrl: 'templates/tab-friends.html',
          controller: 'FriendsCtrl'
        }
      }
    })
    .state('tab.friend-detail', {
      url: '/friend/:friendId',
      views: {
        'tab-friends': {
          templateUrl: 'templates/friend-detail.html',
          controller: 'FriendDetailCtrl'
        }
      }
    })

    .state('tab.account', {
      url: '/account',
      views: {
        'tab-account': {
          templateUrl: 'templates/tab-account.html',
          controller: 'AccountCtrl'
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('dashboard');
});

