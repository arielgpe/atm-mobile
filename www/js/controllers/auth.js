'use strict';

app.controller('LoginController', function($scope, $location, $ionicViewService, $ionicPopup){
	$scope.login = {
		username: '',
		password: ''
	};
	$scope.rememberme = false;

	$scope.remember = function() {
		$scope.rememberme = !$scope.rememberme;
		localStorage.setItem('rememberme', $scope.rememberme);
	}

	$scope.submitLogin = function() {

		if ($scope.login.username.toLowerCase() === 'admin' && $scope.login.password.toLowerCase() === 'admin1234') {
			localStorage.setItem("KEEP_LOGIN", "Lostigueres");

			$ionicViewService.nextViewOptions({
				disableBack: true
			});
			$location.path('dashboard').replace();

		} else if ($scope.login.username === '' || $scope.login.password === ''){
			$ionicPopup.alert({
				title: 'Oops!',
				template: '<p style="text-align: center">fields cannot be blank</p>'
			});
		} else {
			$ionicPopup.alert({
				title: 'Uh Oh!',
				template: '<p style="text-align: center">unknown username or password</p>'
			});
		}
		// LoginService.save($scope.login, function(data) {
		// 	console.log(data);
		// 	if (data.success) {
		// 		$ionicViewService.nextViewOptions({
		// 				disableBack: true
		// 			});
		// 		$location.path('dashboard').replace()
		// 	} else {
		// 		// console.log("No es de los mios")
		// 	}
		// });
	};
});

app.controller('RegisterController', function($scope, $location, $ionicViewService, RegisterService){
	$scope.register = {
		username: '',
		password: '',
		confirm: '',
		email: ''
	};

	$scope.registerSubmit = function() {
		if ($scope.register.password != $scope.register.confirm) {
			console.log("Passwords do not match!")
		} else {
			RegisterService.save($scope.register, function(data) {
				// console.log(data.success)
				if (data.success) {
					$ionicViewService.nextViewOptions({
						disableBack: true
					});
					$location.path('login').replace();
				} else {
					console.log(data.error);
				}
			});
		}
	};
});

app.controller('ForgotController', function($scope, $location, $ionicPopup){
	$scope.forgot = {
		usernameMail: ''
	};

	$scope.submitForgot = function() {
		if ($scope.forgot.usernameMail != '') {
			$ionicPopup.alert({
				title: 'Yes!',
				template: "<p style='text-align: center'>An email has been send to the credentials '" + $scope.forgot.usernameMail + "', thanks!</p>"
			}).then(function(result) {
				if (result == true) {
					$location.path('login');
				}
			});
		} else {
			$ionicPopup.alert({
				title: 'Oops!',
				template: "<p style='text-align: center'>Field cannot be blank</p>"
			});
		}
	}
});
