'use strict';
//This services make it possible to scanner on a recursive way.

app.factory('Scanner', function($cordovaBarcodeScanner, $cordovaToast){
    return function scan() {
        $cordovaBarcodeScanner.scan().then(function (imageData) {
            if (imageData.cancelled) {
                return null;
            } else {
                $cordovaToast.show(imageData.text + '\n' + imageData.format, 'long', 'bottom')
                    .then(function (success) { },

                    function (error) {

                    });
                console.log(imageData.text + '\n' + imageData.format);

                return scan();
            }
        }, function (err) {
            $cordovaToast.show(err, 'long', 'bottom').then(function (success) {

            }, function (error) {

            });
            return null;
        });
    };
});

app.factory('ScannerLock', function ($cordovaPinDialog, Scanner, $ionicPopup, $location) {
    return function scan() {
        var pinPassword = localStorage.getItem('pinPassword');
        $cordovaPinDialog.prompt('Please insert your password', function (results) {
            if (results.buttonIndex == 1) {
                console.log(results.input1);
                if (results.input1 == pinPassword) {
                    $location.path('empty');
                    Scanner();
                } else {
                    $ionicPopup.alert({
                        title: 'Oops!',
                        template: "<p style='text-align: center'>Wrong Password</p>"
                    });
                }
            } else {
                console.log('cancelled')
            }
        }, 'Security Password');
    };
});